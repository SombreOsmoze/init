//
//  main.swift
//  init
//
//  Created by Marcus Florentin on 04/11/2018.
//

import Foundation

print("Bienvenue dans init")

/// Handle program argument

print("Quel partie voulez vous lancer ? Network, System, Scripting ?")

let response = readLine()

guard let part = choosePart(from: response!) else {
	fatalError("Can't read part")
}

try exo(for: part)


