//
//  FileManagement.swift
//  init
//
//  Created by Marcus Florentin on 04/11/2018.
//

import Foundation


enum Part: String {
	case network = "network"
	case system = "system"
	case scripting = "scripting"
}

let initManager = FileManager()

func choosePart(from response: String) -> Part? {
	if response.lowercased().contains("net") {
		return Part.network
	} else if response.lowercased().contains("sys") {
		return Part.system
	} else if response.lowercased().contains("scr") {
		return Part.scripting
	} else {
		return nil
	}
}


@discardableResult func exo(for part: Part) throws -> [String] {

	var exos = try initManager.contentsOfDirectory(atPath: part.rawValue)
	print("Exercices dans la partie \(part.rawValue) :")
	exos.sort()
	exos.forEach({ print($0) })
	return exos
}
 
